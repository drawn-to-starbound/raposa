```yaml
########################################
#                                      #
# THIS PROJECT IS A WORK-IN-PROGRESS!  #
#                                      #   
########################################

#*Do not expect this mod to be feature-complete, or free of bugs.*
#----------------------------------------
#
#  _____     ______     ______     ______    
# /\  __-.  /\  ___\   /\  ___\   /\  ___\   
# \ \ \/\ \ \ \  __\   \ \___  \  \ \ \____  
#  \ \____-  \ \_____\  \/\_____\  \ \_____\ 
#   \/____/   \/_____/   \/_____/   \/_____/ 
                                                            
WorkshopDescription: |+
    Meet the Raposa, a fledgling race of fox-like people believing to have been "Drawn to Life" by their Creator's hand!
    The Raposa are a peace-loving race that, until recently, have been tentative to explore beyond their local group. Homebound, Raposa focused their studies on Magic, a science often overlooked by their space-faring peers, becoming experts. Now eager to spread this knowledge, Raposa have petitioned to join the Terene Protectorate and are in the early stages of colonizing new star systems. However, such a feat is not without difficulty: Gaining acceptance in the greater universal society has been slow coming.
    
    This mod adds the Raposa from Drawn to Life as a fully playable race, complete with full dialog support, a custom ship and mech, new encounters, furniture, blocks, music, lore, and more!
    
    FAQ:
    
    Q: I installed the mod, but I don't see Raposa on the character creation screen.
    A: Install a race extender mod (Like Xbawks Character Extender). This is required to play as any modded race.

    Q: Are there any race-specific items included in this mod?
    A: No, aside from basic things such as armor, all items can be made/purchased by other races.
    
    Q: How do I obtain the blocks and objects added by this mod?
    A: Occasionally in your travels, you will discover various Raposa villages. Use the Warp Gate in a village to be taken to the Raposa homeworld, and locate one of the local shops!
    
    Q: Where can each village be found?
    A: 

    Q: Where can I find the IDs for items added by this mod?
    A: On the Gitlab page for this project: https://gitlab.com/drawn-to-starbound/raposa

#  __     __   __     ______   ______    
# /\ \   /\ "-.\ \   /\  ___\ /\  __ \   
# \ \ \  \ \ \-.  \  \ \  __\ \ \ \/\ \  
#  \ \_\  \ \_\\"\_\  \ \_\    \ \_____\ 
#   \/_/   \/_/ \/_/   \/_/     \/_____/ 
ItemList:
    Materials: # This mod uses Material IDs 3775 - 3875. (https://starbounder.org/Modding:Materials:Mods)
      LastUsedID: 3782
      - "raposashipwall 3775"
      - "raposacobblestone 3776"
      - "raposaplaster 3777    (sandstonematerial-1,clay-1)"
      - "raposaglassplatform 3778"
      - "raposaroof 3779   (orangedye-1,firedclay-1)"
      - "raposaslopedglass 3780    (glass-1,silverbar-1)"
      - "raposadarkslopedglass 3781    (glass-1,ironbar-1)"
      - " 3782"
      - "raposacarpet 3783     (string-4,timber-1)"
      - "raposawallmolding1 3784 (baseboard-1)"
      - "raposawallmolding2 3785 (baseboard-1)"
      - "raposapaintblack 3786 (paper-1,blackdye-1)"
      - "raposapaintblue 3787 (paper-1,bluedye-1)"
      - "raposapaintbrown 3788 (paper-1,browndye-1)"
      - "raposapaintcyan 3789 (paper-1,bluedye-1)"
      - "raposapaintgreen 3790 (paper-1,greendye-1)"
      - "raposapaintorange 3791 (paper-1,orangedye-1)"
      - "raposapaintpurple 3792 (paper-1,purpledye-1)"
      - "raposapaintred 3793 (paper-1,purpledye-1)"
      - "raposapaintwhite 3794 (paper-1,whitedye-1)"
      - "raposapaintyellow 3795 (paper-1,yellowdye-1)"
      - "raposamarble 3796 (paper-1,yellowdye-1)"
      - "raposablackmarble 3797 (raposamarble-1, blackdye-1)"
      - "raposawatersongfence 3798 (raposamarble-1)"
      - "raposatile 3799 (firedclay-1)"
      - "raposatilemsall 3800 (firedclay-1)"
      - "raposafancywindow 3801 (glassmaterial-2,baseboard-1)"
      - "raposabigplanks 3802 (timber-2)"
      - "raposawallshelf 3803 (timber-4, bookpiles-1)"
    Objects:
      Ship:
        - raposashiplockerTier0
        - raposashiplocker
        - raposatechstationTier0
        - raposatechstation
        - raposashiphatch
        - raposashipdoor
        - raposateleporter
        - raposateleporterTier0
        - raposafuelhatch
        - brokenraposafuelhatch
        - raposastoragelocker
      Furniture:
        - flagraposa
        - raposasignup
        - raposawarpgate
        - raposawarpgatebroken
        - raposamusicbox25
        Capital:
          - raposarounddoorgreen
          - raposarounddoorblue
          - raposarounddoorplain
          - raposarounddoorpurple
          - raposarounddoorred
          - raposarounddooryellow
          - raposablueawning
          - raposagenericbed1
          - rapostandingturret #Eternal Flame
          - raposaceilingfan
          - raposapropeller
          - raposapennant1
          - raposapennant1-nocollide
          - raposapennant2
          - raposapennant2-nocollide
          - raposapennant3
          - raposapennant3-nocollide
          - raposapennant4
          - raposapennant4-nocollide
          - raposapennant5
          - raposapennant5-nocollide
          - raposaottoman
          - 
        Void:
          - raposamegagate
        Watersong:
          - raposawickerchair
          - raposawickerseat
          - raposasmallrosepot
          - raposatallrosepot
          - watersonglamppost
          - raposapaintingwatersong1
          - raposaelegantchandelier
          - raposagrandpiano
      Armor:
        Cosmetic:
          - raposalegslongskirt # Female only - Awkward run animation
          - raposalegscargo
          - raposalegssimplepants
          - raposachestshoulderless
          - raposachestovershirt
          - raposachestvnecktshirt
          - raposachestlayeredhoodie
          - raposachestoveralls
        Functional:
          Tier1: #Explorer's Set
            - raposatier1head
            - raposatier1chest
            - raposatier1pants
          Tier2: #Foreman's Set
            - raposatier2head
            - raposatier2chest
            - raposatier2pants
          Tier3: #Officer's Set
            - raposatier3head
            - raposatier3chest
            - raposatier3pants
          Tier3: #Agent's Set
            - raposatier4head
            - raposatier4chest
            - raposatier4pants
          Tier5:
            Manipulator: #Attack  - Salem Set
              - raposatier5mhead
              - raposatier5mchest
              - raposatier5mpants
            Accelerator: [] #Energy - SPUD Set (Ranger)
              - raposatier5ahead
              - raposatier5achest
              - raposatier5apants
            Separator: [] #Health - Mage Set (Normal)
              - raposatier5shead
              - raposatier5schest
              - raposatier5spants
          Tier6:
            Manipulator: #Attack  - Wilfre Set
              - raposatier6mhead
              - raposatier6mchest
              - raposatier6mpants
            Accelerator: [] #Energy - SPUD Set (Chief)
              - raposatier6ahead
              - raposatier6achest
              - raposatier6apants
            Separator: [] #Health - Mage Set (Arch)
              - raposatier6shead
              - raposatier6schest
              - raposatier6spants
          NPC:
              - raposamerchantchest
              - raposamerchantpants
        Weapons:
            Toy:
              - raposasnowgun 
            Unique:
              - raposaacornblaster
            Special:
              - raposamagegun #Invisible gun for mages
    Codex:
      - raposamagicbook-codex
      - raposamagicstudy1-codex
      - raposamagicstudy2-codex
      - raposamagicstudy3-codex
      - raposausermanual1-codex
      - raposasoulbook1-codex
    Crops:
      - raposabanyaseeds
      - raposawildkaorinseed
    Food:
      - raposabanya
      - raposakaorin
      - raposaboiledkaorin
      - raposapancakes
      - raposascones
      - raposasconeskaorin
      - raposabanyapie
      - raposaultimatejuice # Garnishes start
      - raposameatpie
      - raposahamburger
      - raposacheeseburger
      - raposabaconpancakes
      - raposacarrotsoup
      - raposadesertsalsa
      - raposaeggshootsalad
      - raposafishdumplings
      - raposafishpie
      - raposafishstew
      - raposafruitsalad
      - raposagardensalad
      - raposahotbone
      - raposahothothotpot
      - raposameatdumplings
      - raposameatstew
      - raposamushroombread
      - raposamushroomquiche
      - raposamushroomrice
      - raposaoceansalsa
      - raposaomelette
      - raposapasty
      - raposapearlpeabroth
      - raposapearlpearisotto
      - raposapineapplepizzaslice
      - raposapizzaslice
      - raposaquichelorraine
      - raposaroastdinner
      - raposasaltsalad
      - raposaseafoodgratin
      - raposaspecialrice
      - raposaspicyfeathercrown
      - raposaspicyribs
      - raposastuffedtomato
      - raposatomatosoup
      - raposawartweedstew

#   ______   ______                _____     ______    
#  /\__  _\ /\  __ \     ______   /\  __-.  /\  __ \   
#  \/_/\ \/ \ \ \/\ \   /\_____\  \ \ \/\ \ \ \ \/\ \  
#     \ \_\  \ \_____\  \/_____/   \ \____-  \ \_____\   
#      \/_/   \/_____/              \/____/   \/_____/ 
############################### ESSENTIAL 
# This is the backbone of the mod. 
# Everything here must at least be complete 
# before the race is fully playable.
#########################################

Raposa:
    CharacterCreate:
        Colors: Complete ✅
        Clothes:
            Male:
            Female:
        Hair: 
            Male: Complete ✅
            Female: Complete ✅
    Dialog: 
    Cinematics:
        Teleport:
            Beam: Complete ✅
            Mech: 
            Respawn: Complete ✅
    Sound: Complete ✅
    Names: Complete ✅
    
Ship:
    Upgrades: Complete ✅
    Cinematics: Complete ✅
    Interactable: Complete ✅
    SAIL: Complete ✅
    Pet: Complete ✅
    
Lore:
    Starter: Complete ✅
    

Objects:
    Armor: Complete ✅ 
    Flag: Complete ✅
    Mech: 
    
Interface: 
    - "interface/scripted/techupgrade/*" ✅
 
 
--------- 
Misc:
  - Add more lore
  - Add new merchant pools
  - 
 
############################### EXPANSION 
# This adds extra content to the mod.
# This is not required for launch, 
# but makes the race more fun.
#########################################
Objects:
    Furniture: 
      - Jowee/Mari plushes ✅
      - Eternal Flame ✅
      - Book of Life
      - TurtleRock statue/plush
      - Smelly flower
      - BanyaCrystals
      - Rapo fence ✅
      - Color bottles
      - Raposa ears for other species
      - Ship furniture
      
    RacialWeapons: 
      - Brush Spear (Shadow goo special?)
      - Healing wand (One handed)
      - Invisible gun (debug/mages only)
Materials:
    - Path blocks
    - Windows
    - Carpet blocks
    - Marble blocks

NPCs:
    Tenants: 
        -Villagers
        -Guards
        -Mage
        -Mayor
        -Chef
        -Thief
        -Officer
        -Doctor
        -Pirates
        -Crazy person
        -Vampire
    Generic:
    Dialog: 

    
Generation:
    Villages:
        Capital: [Fairy Lights, abandonned variant, easy worlds]
        Watersong:
        Lavasteam:
        Galactic Jungle:

Lore: 
    - Apex experiments
    - Newspaper
    - Staff/gate user manual
    - Raposa History: Formation of mages
    - Raposa History: Invention of Warp Gates
    - Raposa History: Origins of Shadow
    - Raposa History: History of SPUD

#  ______     ______     ______     _____     __     ______  
# /\  ___\   /\  == \   /\  ___\   /\  __-.  /\ \   /\__  _\ 
# \ \ \____  \ \  __<   \ \  __\   \ \ \/\ \ \ \ \  \/_/\ \/ 
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \____-  \ \_\    \ \_\ 
#   \/_____/   \/_/ /_/   \/_____/   \/____/   \/_/     \/_/ 
                                                           
# SHIP: https://community.playstarbound.com/resources/soma-explorer-ships.3147/ [Modified] (CC BY-NC-SA 3)
# Improved Turrets code (used in Eternal Flame object) https://community.playstarbound.com/resources/improved-turrets.2451/
# Special Thanks: BakiBot, Wazamaga, Fishoni, HoodieBlue, DylanDude120, thirsty_plant, and PlumpFeline
                                                           
#  __   __     ______     ______   ______     ______    
# /\ "-.\ \   /\  __ \   /\__  _\ /\  ___\   /\  ___\   
# \ \ \-.  \  \ \ \/\ \  \/_/\ \/ \ \  __\   \ \___  \  
#  \ \_\\"\_\  \ \_____\    \ \_\  \ \_____\  \/\_____\ 
#   \/_/ \/_/   \/_____/     \/_/   \/_____/   \/_____/ 

# Raposa are: Magical, Optimistic, Cautious                                             
# Female/Male raposa are seperate races
# Remove unused files from tiles/materials/ 
# Update furcolor list
# Scan for old files
# Organize items by village
# Finish Raposa Roof material
# Roses too dark
# Blocks at least need recipes       
# Corrupted regions
# Add recipes for all items/blocks
# LADDER LIKE THE COPPER LADDER
# Some clothing is missing from the spinning wheel
            
#  ______     __  __     ______     ______    
# /\  == \   /\ \/\ \   /\  ___\   /\  ___\   
# \ \  __<   \ \ \_\ \  \ \ \__ \  \ \___  \  
#  \ \_____\  \ \_____\  \ \_____\  \/\_____\ 
#   \/_____/   \/_____/   \/_____/   \/_____/ 
#Chandelier sometimes has an invalid hitbox
#Turning off Eternal Flame repeats sounds
# Scarf shirt displays no scarf on female models
```
