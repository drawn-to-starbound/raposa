function init()
  object.setInteractive(true)
  animator.setAnimationState("brokengate", "stop")
  self.sounds = config.getParameter("sounds", {})
  animator.setSoundPool("noise", self.sounds)
end

function onInteraction(args)
  animator.setAnimationState("brokengate", "cycle")
  if #self.sounds > 0 then
    animator.playSound("noise")
  end
end

function onNpcPlay(npcId)
  local interact = config.getParameter("npcToy.interactOnNpcPlayStart")
  if interact == nil or interact ~= false then
    onInteraction()
  end
end
