-- based on wellfed
function init()
  world.sendEntityMessage(entity.id(), "queueRadioMessage", "raposakaorinsickness")

  animator.setParticleEmitterOffsetRegion("drips", mcontroller.boundBox())
  animator.setParticleEmitterActive("drips", true)

  script.setUpdateDelta(5)

  self.tickDamagePercentage = 0.010
  self.tickTime = 10.0
  self.tickTimer = self.tickTime
  self.movementModifiers = config.getParameter("movementModifiers", {})
  
end

function update(dt)
  self.tickTimer = self.tickTimer - dt
  if self.tickTimer <= 0 then
    self.tickTimer = self.tickTime
    status.applySelfDamageRequest({
        damageType = "IgnoresDef",
        damage = math.floor(status.resourceMax("health") * self.tickDamagePercentage) + 1,
        damageSourceKind = "poison",
        sourceEntityId = entity.id()
      })
  end
  mcontroller.controlModifiers(self.movementModifiers)
  effect.setParentDirectives(string.format("fade=00AA00=%.1f", self.tickTimer * 0.04))
end

function uninit()

end
